// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "WorldInteraction_Char.h"
#include "NPC_WorldInteraction.generated.h"

/**
 * 
 */
UCLASS()
class NINJABOLT_API ANPC_WorldInteraction : public AWorldInteraction_Char
{
	GENERATED_UCLASS_BODY()


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Custom , Movement")
		bool bWillFly;
	bool bHasBeenHit;
	FVector flightDirection;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Custom , Movement")
		float flightSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Custom , Movement")
		float flightZSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Custom , Damage")
		float FuelReductionValue;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Custom , Damage")
		float SpeedReductionValue;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Custom , Damage")
		int32 ScoreReductionValue;

	virtual void ReceiveHit(class UPrimitiveComponent* MyComp, AActor* Other, class UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit) override;
	virtual void Tick(float DeltaSeconds) override;

	void EndFlight();
};
