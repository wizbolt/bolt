// Fill out your copyright notice in the Description page of Project Settings.

#include "NinjaBolt.h"
#include "FuelImmune_Pickup.h"




AFuelImmune_Pickup::AFuelImmune_Pickup(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	duration = 5.f;
}

void AFuelImmune_Pickup::PickupAction(APlayer_Char* hitPlayer)
{
	hitPlayer->SetFuelImmunity(duration);
}