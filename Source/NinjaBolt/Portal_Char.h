// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Ninja_Char.h"
#include "WorldInteraction_Char.h"

#include "Vehicle_Char.h"

#include "Portal_Char.generated.h"

/**
 * 
 */
UCLASS()
class NINJABOLT_API APortal_Char : public AWorldInteraction_Char
{
	GENERATED_UCLASS_BODY()
	
	
	

	virtual void BeginOverlapPlayer(APlayer_Char* otherPlayer) override;

	void ForceActorTransform(APlayer_Char* otherPlayer);
};
