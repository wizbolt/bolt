// Fill out your copyright notice in the Description page of Project Settings.

#include "NinjaBolt.h"
#include "NPC_WorldInteraction.h"





ANPC_WorldInteraction::ANPC_WorldInteraction(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bWillFly = true;
	bHasBeenHit = false;
	flightDirection = { 0, 0, 0 };
	flightSpeed = 25.f;
	flightZSpeed = 75.f;

	FuelReductionValue = 0.f;
	SpeedReductionValue = 0.f;
	ScoreReductionValue = 0;

	CharacterMovement->MaxWalkSpeed = 5000000;
	CharacterMovement->MaxFlySpeed = 5000000;
}


void ANPC_WorldInteraction::ReceiveHit(class UPrimitiveComponent* MyComp, AActor* Other, class UPrimitiveComponent* OtherComp,
	bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::ReceiveHit(MyComp, Other, OtherComp, bSelfMoved, HitLocation, HitNormal, NormalImpulse, Hit);
	if (CheckValidPlayer(Other) && !bHasBeenHit)
	{
		APlayer_Char* casted_Char = Cast<APlayer_Char>(Other);
		if (casted_Char)
		{
			if (FuelReductionValue > 0.f)
			{
				casted_Char->ReduceFuel(FuelReductionValue);
			}
			if (SpeedReductionValue > 0.f)
			{
				casted_Char->ReduceSpeed(SpeedReductionValue);
			}
			if (ScoreReductionValue > 0.f)
			{
				casted_Char->ReduceScore(ScoreReductionValue);
			}
		}
		flightDirection = GetActorLocation() - Other->GetActorLocation();
		flightDirection.Z = flightZSpeed;
		/*flightDirection.X *= flightSpeed;
		flightDirection.Y *= flightSpeed;
		flightDirection.Z = flightSpeed * 5;*/
		bHasBeenHit = true;
		GetWorldTimerManager().SetTimer(this, &ANPC_WorldInteraction::EndFlight, 5.f, false);
	}
}

void ANPC_WorldInteraction::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	
	if (bHasBeenHit && bWillFly)
	{
		float tempSpeed = flightSpeed * DeltaSeconds;
		FVector tempVec = flightDirection*tempSpeed;
		FVector tempVec2 = GetActorLocation() + tempVec;
		SetActorLocation(tempVec2, false);
		DisplayMessage(tempVec.ToString(), DeltaSeconds, FColor::Green);
	}
}

void ANPC_WorldInteraction::EndFlight()
{
	Destroy();
}