// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Ninja_Char.h"

#include "Player_Char.h"

#include "WorldInteraction_Char.generated.h"

/**
 * 
 */
UCLASS()
class NINJABOLT_API AWorldInteraction_Char : public ANinja_Char
{
	GENERATED_UCLASS_BODY()



	/*Collision Override Functions*/
	virtual void ReceiveHit(class UPrimitiveComponent* MyComp, AActor* Other, class UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit) override;
	virtual void ReceiveActorBeginOverlap(AActor* OtherActor) override;
	virtual void ReceiveActorEndOverlap(AActor* OtherActor) override;

	/*Checks if hit actor is valid*/
	bool CheckValidPlayer(AActor* otherActor);

	virtual void HitPlayer(APlayer_Char* otherPlayer);
	virtual void BeginOverlapPlayer(APlayer_Char* otherPlayer);
	virtual void EndOverlapPlayer(APlayer_Char* otherPlayer);
	virtual void BelowPlayer(APlayer_Char* otherPlayer);
};
