// Fill out your copyright notice in the Description page of Project Settings.

#include "NinjaBolt.h"
#include "Fuel_Pickup.h"


AFuel_Pickup::AFuel_Pickup(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	value = 10;
}


void AFuel_Pickup::PickupAction(APlayer_Char* hitPlayer)
{
	hitPlayer->AddFuel(value);
}