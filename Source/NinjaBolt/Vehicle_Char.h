// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Player_Char.h"

#include "WorldInteraction_Char.h"

#include "Vehicle_Char.generated.h"

/*List of possible states*/
UENUM(BlueprintType, Category = "custom")
enum Transform_State
{
	State_Transforming,
	State_Car,
	State_Plane,
	State_Bike
};

/**
 * 
 */
UCLASS()
class NINJABOLT_API AVehicle_Char : public APlayer_Char
{
	GENERATED_UCLASS_BODY()

	bool bAutoAccelerate;//Auto accelerate only when the game starts
	bool bIsCollidingWithWalls;//returns true when colliding with walls
	bool bIsTurning;//returns true when turning

	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Custom")
		bool bDeath;//When player is dead
	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Custom")
		float warningHeightZ;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Custom")
		float killHeightZ;
	
	/*probably depreceated | For use only when using a single mesh per transformation*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Custom")
		TArray<USkeletalMesh*> MeshList;
	/*probably depreceated | For use only when using a single mesh per transformation*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Custom")
		TArray<UClass*> AnimationList;

	/*States of the actor*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Custom, State")
		int32 currentState;
	int32 prevState;//The state from before going into transformation
	int32 heldState;//The state to transform to if the player inputs a new state during transformation
	bool bStillInAir;//returns true if still falling after transforming from plane
	bool bIsTakingOff;//returns true when transforming into a plane
	bool bCanTransform;//returns true when able to transform
	bool bIsTransforming;//returns true if in transformation anim

	/*delay for when the player transforms*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Custom")
		float TransformationTimeoutDelay;
	/*delay for after the player transforms*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Custom")
		float PostTransformDelay;

	/*Bool keys for input controls*/
	UPROPERTY(BlueprintReadWrite, category = "Custom, Input")
		bool bKeyTurnLeft_Car;
	UPROPERTY(BlueprintReadWrite, category = "Custom, Input")
		bool bKeyTurnRight_Car;
	UPROPERTY(BlueprintReadWrite, category = "Custom, Input")
		bool bKeyTurnLeft_Plane;
	UPROPERTY(BlueprintReadWrite, category = "Custom, Input")
		bool bKeyTurnRight_Plane;
	UPROPERTY(BlueprintReadWrite, category = "Custom, Input")
		bool bKeyPitchUp_Plane;
	UPROPERTY(BlueprintReadWrite, category = "Custom, Input")
		bool bKeyPitchDown_Plane;
	UPROPERTY(BlueprintReadWrite, category = "Custom, Input")
		bool bKeyTurnLeft_Bike;
	UPROPERTY(BlueprintReadWrite, category = "Custom, Input")
		bool bKeyTurnRight_Bike;
	UPROPERTY(BlueprintReadWrite, category = "Custom, Input")
		bool bKeyWheelie_Bike;

	/*Rotation that is set by trace*/
	FRotator supposedRotation;

	/*Custom rotation for Actor*/
	FRotator currentRotation;

	/*Custom Rotation for Mesh*/
	FRotator meshCurrentRotation;

	/*Speed reduction when over max speed*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Custom,Movement")
		float overSpeedDecay;

	/*Speed Reduction upon Collision*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Custom,Movement")
		float collisionSpeedReduction;
	/*Speed Reduction when Turning the Car*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Custom,Movement")
		float carTurnSpeedReduction;
	/*Speed Reduction when Turning the Bike*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Custom,Movement")
		float bikeTurnSpeedReduction;

	/*Car Acceleration*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Custom, Movement")
		float carAcceleration;
	/*Plane Acceleration*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Custom, Movement")
		float planeAcceleration;
	/*Bike Acceleration*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Custom, Movement")
		float bikeAcceleration;

	/*Car Max Speed*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Custom, Movement")
		float carMaxSpeed;
	/*Plane Max Speed*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Custom, Movement")
		float planeMaxSpeed;
	/*Bike Max Speed*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Custom, Movement")
		float bikeMaxSpeed;

	/*Car Yaw Rate*/
	UPROPERTY(EditAnywhere, category = "Custom, Movement")
		float carYawRate;
	/*Plane Yaw Rate*/
	UPROPERTY(EditAnywhere, category = "Custom, Movement")
		float planeYawRate;
	/*Bike Yaw Rate*/
	UPROPERTY(EditAnywhere, category = "Custom, Movement")
		float bikeYawRate;

	/*Plane Pitch Rate*/
	UPROPERTY(EditAnywhere, category = "Custom, Movement")
		float planePitchRate;
	/*Plane Max Pitch*/
	UPROPERTY(EditAnywhere, category = "Custom, Movement")
		float planeMaxPitch;
	/*Plane Roll Rate*/
	UPROPERTY(EditAnywhere, category = "Custom, Movement")
		float planeRollRate;
	/*Plane Max Roll*/
	UPROPERTY(EditAnywhere, category = "Custom, Movement")
		float planeMaxRoll;

	/*Plane Current Pitch*/
	float planeCurrentPitch;
	/*Plane Current Roll*/
	float planeCurrentRoll;

	/*Mesh Pitch*/
	float meshPitch;
	FVector boundingBoxVector;

	/*Fuel Consumption for the Car*/
	UPROPERTY(EditAnywhere, category = "Custom, Fuel")
		float carFuelConsumption;
	/*Fuel Consumption for the Plane*/
	UPROPERTY(EditAnywhere, category = "Custom, Fuel")
		float planeFuelConsumption;
	/*Fuel Consumption for the Bike*/
	UPROPERTY(EditAnywhere, category = "Custom, Fuel")
		float bikeFuelConsumption;



	/*Called when the game starts*/
	UFUNCTION(BlueprintCallable, Category = "Custom, Event")
		void StartVehicle();
	

	/*Start the transformation of the vehicle*/
	UFUNCTION(BlueprintCallable, Category = "Custom, State")
		bool StartTransform();
	/*Called to change the state of heldState*/
	UFUNCTION(BlueprintCallable, Category = "Custom, State")
		void SetNextState(Transform_State nextState);
	/*Called when transformation time is up*/
	void TransformTimeout();
	/*BP implemented*/
	UFUNCTION(BlueprintImplementableEvent, Category = "Custom, State")
		void StartMeshChange();
	/*Called to change the state of the vehicle*/
	void ChangeState();
	/*BP implemented*/
	UFUNCTION(BlueprintImplementableEvent, Category = "Custom, State")
		void CustomMeshChange(int32 meshNum);
	/*Automatically called after transformation is done*/
	void PostTransform();
	/*BP implemented*/
	UFUNCTION(BlueprintImplementableEvent, Category = "Custom, State")
		void PostTransformBP();
	/*Called to enable the player to transform again*/
	void EnableTransformation();
	/*BP implemented*/
	UFUNCTION(BlueprintImplementableEvent, Category = "Custom, State")
		void DisplayStateTransition(int32 prevState, int32 newState);

	void ForceTransform(Transform_State forceState);
	/*BP implemented*/
	UFUNCTION(BlueprintImplementableEvent, Category = "Custom, State")
		void ForceTransformBP();
	void ForceRandomTransform();
	
	//Called during tick for each various states
	void UpdateTransformationInput(float DeltaSeconds);
	void UpdateCarInput(float DeltaSeconds);
	void UpdatePlaneInput(float DeltaSeconds);
	void UpdateBikeInput(float DeltaSeconds);
	


	/*Called during Tick to set the player's rotation during the game*/
	void SetRotationViaTrace();

	void DampenRotation(float DeltaSeconds);

	//Engine functions that have to be overriden
	virtual void Tick(float DeltaSeconds) override;
	virtual void BeginPlay() override;

	/*Collision Override Functions*/
	virtual void ReceiveHit(class UPrimitiveComponent* MyComp, AActor* Other, class UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit) override;
	virtual void ReceiveActorBeginOverlap(AActor* OtherActor) override;
	virtual void ReceiveActorEndOverlap(AActor* OtherActor) override;
};
