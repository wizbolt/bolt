// Fill out your copyright notice in the Description page of Project Settings.

#include "NinjaBolt.h"
#include "Player_Char.h"


APlayer_Char::APlayer_Char(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	currentScore = 0;

	currentSpeed = 0.f;
	currentBoostedSpeed = 0.f;
	boostedSpeedDecay = 30.f;
	bIsBoosted = false;

	currentFuel = 0.f;
	maxFuel = 0.f;

	bIsFuelImmune = false;
}

void APlayer_Char::AddScore(int32 inScore)
{
	currentScore += inScore;
}

void APlayer_Char::ReduceScore(int32 inScore)
{
	currentScore -= inScore;
	if (currentScore <= 0)
	{
		currentScore = 0;
	}
}

void APlayer_Char::AddBoostedSpeed(float boostedSpeed)
{
	if (currentBoostedSpeed >= boostedSpeed)
	{
		return;
	}
	currentBoostedSpeed = boostedSpeed;
	bIsBoosted = true;
}

void APlayer_Char::ReduceSpeed(float reducedSpeed)
{
	currentSpeed -= reducedSpeed;
	if (currentSpeed <= 0.f)
	{
		currentSpeed = 0.f;
	}
}

void APlayer_Char::AddFuel(float inFuel)
{
	currentFuel += inFuel;
	if (currentFuel > maxFuel)
	{
		currentFuel = maxFuel;
	}
}

void APlayer_Char::ReduceFuel(float inFuel)
{
	currentFuel -= inFuel;
	if (currentFuel <= 0.f)
	{
		currentFuel = 0.f;
	}
}

void APlayer_Char::SetFuelImmunity(float duration)
{
	bIsFuelImmune = true;
	GetWorldTimerManager().SetTimer(this, &APlayer_Char::StopFuelImmunity, duration, false);
}

void APlayer_Char::StopFuelImmunity()
{
	bIsFuelImmune = false;
}

void APlayer_Char::SetInvincibility(float duration)
{
	bIsInvincible = true;
	GetWorldTimerManager().SetTimer(this, &APlayer_Char::StopInvincibility, duration, false);
}

void APlayer_Char::StopInvincibility()
{
	bIsInvincible = true;
}

void APlayer_Char::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	if (bIsBoosted)
	{
		currentBoostedSpeed -= boostedSpeedDecay*DeltaSeconds;
		if (currentBoostedSpeed <= 0.f)
		{
			bIsBoosted = false;
		}
	}
}

