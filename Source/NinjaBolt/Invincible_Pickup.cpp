// Fill out your copyright notice in the Description page of Project Settings.

#include "NinjaBolt.h"
#include "Invincible_Pickup.h"





AInvincible_Pickup::AInvincible_Pickup(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	duration = 5.f;
}

void AInvincible_Pickup::PickupAction(APlayer_Char* hitPlayer)
{
	hitPlayer->SetInvincibility(duration);
}