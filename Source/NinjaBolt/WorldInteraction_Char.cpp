// Fill out your copyright notice in the Description page of Project Settings.

#include "NinjaBolt.h"
#include "WorldInteraction_Char.h"





AWorldInteraction_Char::AWorldInteraction_Char(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}



void AWorldInteraction_Char::ReceiveHit(class UPrimitiveComponent* MyComp, AActor* Other, class UPrimitiveComponent* OtherComp,
	bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::ReceiveHit(MyComp, Other, OtherComp, bSelfMoved, HitLocation, HitNormal, NormalImpulse, Hit);
	if (CheckValidPlayer(Other))
	{
		HitPlayer(Cast<APlayer_Char>(Other));
	}
}

void AWorldInteraction_Char::ReceiveActorBeginOverlap(AActor* OtherActor)
{
	Super::ReceiveActorBeginOverlap(OtherActor);
	if (CheckValidPlayer(OtherActor))
	{
		BeginOverlapPlayer(Cast<APlayer_Char>(OtherActor));
	}
}

void AWorldInteraction_Char::ReceiveActorEndOverlap(AActor* OtherActor)
{
	Super::ReceiveActorEndOverlap(OtherActor);
	if (CheckValidPlayer(OtherActor))
	{
		EndOverlapPlayer(Cast<APlayer_Char>(OtherActor));
	}
}



bool AWorldInteraction_Char::CheckValidPlayer(AActor* otherActor)
{
	APlayer_Char* otherPlayer;
	otherPlayer = Cast<APlayer_Char>(otherActor);
	if (!otherPlayer)
	{
		return false;
	}
	return true;
}



void AWorldInteraction_Char::HitPlayer(APlayer_Char* otherPlayer)
{
}

void AWorldInteraction_Char::BeginOverlapPlayer(APlayer_Char* otherPlayer)
{
}

void AWorldInteraction_Char::EndOverlapPlayer(APlayer_Char* otherPlayer)
{
}

void AWorldInteraction_Char::BelowPlayer(APlayer_Char* otherPlayer)
{
}