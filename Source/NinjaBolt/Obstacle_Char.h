// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "WorldInteraction_Char.h"

#include "Player_Char.h"

#include "Obstacle_Char.generated.h"

/**
 * 
 */
UCLASS()
class NINJABOLT_API AObstacle_Char : public AWorldInteraction_Char
{
	GENERATED_UCLASS_BODY()

	/*Checks if Obstacle has been sprung*/
	bool bAlreadyActivated;

	/*How much speed the player loses upon contact*/
	int32 speedDecreaseValue;

	virtual void HitPlayer(APlayer_Char* otherPlayer) override;
	virtual void BeginOverlapPlayer(APlayer_Char* otherPlayer) override;
	virtual void EndOverlapPlayer(APlayer_Char* otherPlayer) override;
	virtual void BelowPlayer(APlayer_Char* otherPlayer) override;

	void CheckPlayerInvincibility(APlayer_Char* otherPlayer);

	//Functions to be overriden by child obstacles
	virtual void HitInvinciblePlayer();
	virtual void HitNormalPlayer(APlayer_Char* hitPlayer);
};
