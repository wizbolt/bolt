// Fill out your copyright notice in the Description page of Project Settings.

#include "NinjaBolt.h"
#include "Vehicle_Char.h"


AVehicle_Char::AVehicle_Char(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	/*Set default variables found in Super*/
	CharacterMovement->SetMovementMode(MOVE_Falling);
	currentFuel = 60.f;
	maxFuel = 60.f;

	/*Variables for this class IN ORDER from .h file*/
	bAutoAccelerate = false;
	bIsCollidingWithWalls = false;
	bIsTurning = false;

	bDeath = false;
	warningHeightZ = 5000.f;
	killHeightZ = 7500.f;

	currentState = State_Car;
	prevState = -1;
	heldState = -1;
	bStillInAir = false;
	bIsTakingOff = false;
	bCanTransform = false;
	bIsTransforming = false;

	TransformationTimeoutDelay = 1.f;
	PostTransformDelay = 1.f;

	bKeyTurnLeft_Car = false;
	bKeyTurnRight_Car = false;
	bKeyTurnLeft_Plane = false;
	bKeyTurnRight_Plane = false;
	bKeyPitchUp_Plane = false;
	bKeyPitchDown_Plane = false;
	bKeyTurnLeft_Bike = false;
	bKeyTurnRight_Bike = false;
	bKeyWheelie_Bike = false;

	supposedRotation = { 0, 0, 0 };
	currentRotation = { 0, 0, 0 };
	meshCurrentRotation = { 0, 0, 0 };
	currentSpeed = 0.f;

	overSpeedDecay = 30.f;

	collisionSpeedReduction = 240.f;
	carTurnSpeedReduction = 30.f;
	bikeTurnSpeedReduction = 15.f;

	carAcceleration = 30.f;
	planeAcceleration = 30.f;
	bikeAcceleration = 30.f;

	carMaxSpeed = 180.f;
	planeMaxSpeed = 240.f;
	bikeMaxSpeed = 120.f;

	carYawRate = 90.f;
	planeYawRate = 60.f;
	bikeYawRate = 180.f;

	planePitchRate = 75.f;
	planeMaxPitch = 60.f;
	planeRollRate = 60.f;
	planeMaxRoll = 30.f;

	planeCurrentPitch = 0.f;
	planeCurrentRoll = 0.f;

	meshPitch = 0.f;
	boundingBoxVector = { 0, 0, 0 };

	carFuelConsumption = 1.f;
	planeFuelConsumption = 2.f;
	bikeFuelConsumption = 1.5f;
}

void AVehicle_Char::StartVehicle()
{
	bAutoAccelerate = true;
}

bool AVehicle_Char::StartTransform()
{
	if (bCanTransform)
	{
		return false;
	}

	bCanTransform = true;
	bIsTransforming = true;
	prevState = currentState;
	currentState = State_Transforming;
	CharacterMovement->SetMovementMode(MOVE_Flying);
	GetWorldTimerManager().SetTimer(this, &AVehicle_Char::TransformTimeout, TransformationTimeoutDelay, false);
	return true;
}

void AVehicle_Char::SetNextState(Transform_State nextState)
{
	if (!bIsTransforming)
	{
		return;
	}
	heldState = nextState;
	bIsTransforming = false;
}

void AVehicle_Char::TransformTimeout()
{
	bIsTransforming = false;
	ChangeState();
	StartMeshChange();
}

void AVehicle_Char::ChangeState()
{
	/*Start falling when transforming out of plane*/
	if (prevState == State_Plane)
	{
		bStillInAir = true;
	}

	/*Start revving engine when transforming into plane*/
	if (heldState == State_Plane)
	{
		bIsTakingOff = true;
	}

	/*Check if the player put in any new input*/
	if (heldState > 0)
	{
		//Mesh->SetSkeletalMesh(MeshList[heldState - 1]);
		//Mesh->SetAnimInstanceClass(AnimationList[heldState - 1]);
		CustomMeshChange(heldState - 1);
		//DisplayMessage("Mesh and Anim swapped", 3.f, FColor::Green);
	}
	GetWorldTimerManager().SetTimer(this, &AVehicle_Char::PostTransform, TransformationTimeoutDelay, false);
}

void AVehicle_Char::PostTransform()
{
	planeCurrentRoll = GetActorRotation().Roll;
	planeCurrentPitch = GetActorRotation().Pitch;

	//change out of transformation state and reset heldState
	if (heldState > 0)
	{
		currentState = heldState;
	}
	else
	{
		currentState = prevState;
	}
	heldState = -1;
	prevState = -1;

	if (currentState != State_Plane)
	{
		CharacterMovement->SetMovementMode(MOVE_Falling);
	}

	PostTransformBP();
	GetWorldTimerManager().SetTimer(this, &AVehicle_Char::EnableTransformation, PostTransformDelay, false);
}

void AVehicle_Char::EnableTransformation()
{
	bCanTransform = false;
}

void AVehicle_Char::ForceTransform(Transform_State forceState)
{
	if (GetWorldTimerManager().IsTimerActive(this, &AVehicle_Char::TransformTimeout))
	{
		GetWorldTimerManager().ClearTimer(this, &AVehicle_Char::TransformTimeout);
	}
	if (GetWorldTimerManager().IsTimerActive(this, &AVehicle_Char::PostTransform))
	{
		GetWorldTimerManager().ClearTimer(this, &AVehicle_Char::PostTransform);
	}
	if (GetWorldTimerManager().IsTimerActive(this, &AVehicle_Char::EnableTransformation))
	{
		GetWorldTimerManager().ClearTimer(this, &AVehicle_Char::EnableTransformation);
	}
	bCanTransform = true;
	bIsTransforming = false;
	currentState = State_Transforming;
	CharacterMovement->SetMovementMode(MOVE_Flying);
	GetWorldTimerManager().SetTimer(this, &AVehicle_Char::TransformTimeout, TransformationTimeoutDelay, false);
	heldState = forceState;//Force it instead of assigning it to SetNextState
	ForceTransformBP();//will emulate the btn press in Blueprints
}

void AVehicle_Char::ForceRandomTransform()
{
	int randNum = FMath::RandRange(1,3);
	ForceTransform(Transform_State(randNum));
}



void AVehicle_Char::UpdateTransformationInput(float DeltaSeconds)
{
	/*Reset Roll*/
	if (currentRotation.Roll > 0.f)
	{
		currentRotation.Roll -= planeRollRate * DeltaSeconds;
	}
	else if (currentRotation.Roll < 0.f)
	{
		currentRotation.Roll += planeRollRate * DeltaSeconds;
	}
	if (abs(currentRotation.Roll) <= 1.f)
	{
		currentRotation.Roll = 0.f;
	}

	/*TakeOff when transforming into a plane*/
	if (heldState == State_Plane)
	{
		if (currentRotation.Pitch < 10.f)
		{
			currentRotation.Pitch += planePitchRate * DeltaSeconds;
		}
		//TODO: Reset Mesh Rotation here
		if (meshCurrentRotation.Pitch > 0.f)
		{
			meshCurrentRotation.Pitch -= 1.f;
			if (meshCurrentRotation.Pitch <= 1.f)
			{
				meshCurrentRotation.Pitch = 0.f;
			}
		}
		else if (meshCurrentRotation.Pitch < 0.f)
		{
			meshCurrentRotation.Pitch += 1.f;
			if (meshCurrentRotation.Pitch >= -1.f)
			{
				meshCurrentRotation.Pitch = 0.f;
			}
		}
	}
	else /*Reset Pitch*/
	{
		if (currentRotation.Pitch > 0.f)
		{
			currentRotation.Pitch -= planePitchRate * DeltaSeconds;
		}
		else if (currentRotation.Pitch < 0.f)
		{
			currentRotation.Pitch += planePitchRate * DeltaSeconds;
		}
		if (abs(currentRotation.Pitch) <= 1.f)
		{
			currentRotation.Pitch = 0.f;
		}
	}
}

void AVehicle_Char::UpdateCarInput(float DeltaSeconds)
{
	//Detect Car Input
	if (bKeyTurnLeft_Car && !bKeyTurnRight_Car)
	{
		currentRotation.Yaw -= carYawRate * DeltaSeconds;
		bIsTurning = true;
	}
	else if (bKeyTurnRight_Car && !bKeyTurnLeft_Car)
	{
		currentRotation.Yaw += carYawRate * DeltaSeconds;
		bIsTurning = true;
	}
	else
	{
		bIsTurning = false;
	}

	//Set Car Speed
	if (bIsCollidingWithWalls)
	{
		currentSpeed -= collisionSpeedReduction*DeltaSeconds;
		if (currentSpeed < 0.f)
		{
			currentSpeed = 0.f;
		}
	}
	else if (bIsTurning)
	{
		currentSpeed -= carTurnSpeedReduction*DeltaSeconds;
		if (currentSpeed < 0.f)
		{
			currentSpeed = 0.f;
		}
	}
	else if (bAutoAccelerate)
	{
		currentSpeed += carAcceleration*DeltaSeconds;
	}
	SetRotationViaTrace();
}

void AVehicle_Char::UpdatePlaneInput(float DeltaSeconds)
{
	if (bAutoAccelerate)
	{
		currentSpeed += planeAcceleration*DeltaSeconds;
	}
	/*Flight Roll & Yaw*/
	if (bKeyTurnLeft_Plane && !bKeyTurnRight_Plane)
	{
		currentRotation.Yaw -= planeRollRate * DeltaSeconds;
		planeCurrentRoll -= planeRollRate * DeltaSeconds;
		if (planeCurrentRoll < -planeMaxRoll)
		{
			planeCurrentRoll = -planeMaxRoll;
		}
	}
	else if (bKeyTurnRight_Plane && !bKeyTurnLeft_Plane)
	{
		currentRotation.Yaw += planeRollRate * DeltaSeconds;
		planeCurrentRoll += planeRollRate * DeltaSeconds;
		if (planeCurrentRoll > planeMaxRoll)
		{
			planeCurrentRoll = planeMaxRoll;
		}
	}
	else
	{
		if (planeCurrentRoll > 0.f)
		{
			planeCurrentRoll -= planeRollRate *DeltaSeconds;
		}
		else if (planeCurrentRoll < 0.f)
		{
			planeCurrentRoll += planeRollRate * DeltaSeconds;
		}
		if (abs(planeCurrentRoll) < 1.f)
		{
			planeCurrentRoll = 0.f;
		}
	}
	currentRotation.Roll = planeCurrentRoll;

	/*Flight Pitch*/
	if (bKeyPitchUp_Plane && !bKeyPitchDown_Plane)
	{
		planeCurrentPitch += planePitchRate * DeltaSeconds;
		if (planeCurrentPitch > planeMaxPitch)
		{
			planeCurrentPitch = planeMaxPitch;
		}
	}
	else if (bKeyPitchDown_Plane && !bKeyPitchUp_Plane)
	{
		planeCurrentPitch -= planePitchRate * DeltaSeconds;
		if (planeCurrentPitch < -planeMaxPitch)
		{
			planeCurrentPitch = -planeMaxPitch;
		}
	}
	currentRotation.Pitch = planeCurrentPitch;
}

void AVehicle_Char::UpdateBikeInput(float DeltaSeconds)
{
	//Detect Bike Input
	if (bKeyTurnLeft_Bike && !bKeyTurnRight_Bike)
	{
		currentRotation.Yaw -= bikeYawRate * DeltaSeconds;
		bIsTurning = true;
	}
	else if (bKeyTurnRight_Bike && !bKeyTurnLeft_Bike)
	{
		currentRotation.Yaw += bikeYawRate * DeltaSeconds;
		bIsTurning = true;
	}
	else
	{
		bIsTurning = false;
	}

	//Set Bike Speed
	if (bIsCollidingWithWalls)
	{
		currentSpeed -= collisionSpeedReduction*DeltaSeconds;
	}
	else if (bIsTurning)
	{
		currentSpeed -= bikeTurnSpeedReduction*DeltaSeconds;
		if (currentSpeed < 0.f)
		{
			currentSpeed = 0.f;
		}
	}
	else if (bAutoAccelerate)
	{
		currentSpeed += bikeAcceleration*DeltaSeconds;
	}
	SetRotationViaTrace();
}

void AVehicle_Char::SetRotationViaTrace()
{
	/*Init Trace Points Here*/
	float traceLength = -200.f;
	FVector originLocation = GetActorLocation();
	FVector frontTip = originLocation + currentRotation.RotateVector((GetActorForwardVector()*boundingBoxVector.X));
	//frontTip += GetActorUpVector()*(-boundingBoxVector.Z);
	FVector backTip = originLocation + currentRotation.RotateVector((GetActorForwardVector()*(-boundingBoxVector.X)));
	//backTip += GetActorUpVector()*(-boundingBoxVector.Z);
	FVector leftTip = originLocation + currentRotation.RotateVector((GetActorRightVector()*(-boundingBoxVector.Y)));
	FVector rightTip = originLocation + currentRotation.RotateVector((GetActorRightVector()*boundingBoxVector.Y));

	FVector frontEnd = frontTip + (GetActorUpVector()*traceLength);
	FVector backEnd = backTip + (GetActorUpVector()*traceLength);
	FVector leftEnd = leftTip + (GetActorUpVector()*traceLength);
	FVector rightEnd = rightTip + (GetActorUpVector()*traceLength);
	
	/*Hit Results and CollisionQueryParams*/
	FHitResult frontHR;
	FHitResult backHR;
	FHitResult leftHR;
	FHitResult rightHR;

	FCollisionQueryParams CQParams;
	CQParams.bTraceComplex = true;
	CQParams.AddIgnoredActor(this);
	
	/*Trace*/
	bool frontHit = GetWorld()->LineTraceSingle(frontHR, frontTip, frontEnd, ECollisionChannel::ECC_MAX, CQParams);
	bool backHit = GetWorld()->LineTraceSingle(backHR, backTip, backEnd, ECollisionChannel::ECC_MAX, CQParams);
	bool leftHit = GetWorld()->LineTraceSingle(leftHR, leftTip, leftEnd, ECollisionChannel::ECC_MAX, CQParams);
	bool rightHit = GetWorld()->LineTraceSingle(rightHR, rightTip, rightEnd, ECollisionChannel::ECC_MAX, CQParams);
	

	/*If frontHit hits a WorldInteraction object, call for it's BelowPlayer()*/
	AWorldInteraction_Char* hit_WI = Cast<AWorldInteraction_Char>(frontHR.GetActor());
	if (hit_WI)
	{
		hit_WI->BelowPlayer(this);
	}
	
	/*Debug lines*/
	/*if (frontHit)
	{
		DrawDebugLine(GetWorld(), frontTip, frontHR.ImpactPoint, FColor(0, 255, 0), false, -1, 0, 10);
	}
	else
	{
		DrawDebugLine(GetWorld(), frontTip, frontEnd, FColor(255, 0, 0), false, -1, 0, 10);
	}
	if (backHit)
	{
		DrawDebugLine(GetWorld(), backTip, backHR.ImpactPoint, FColor(0, 255, 0), false, -1, 0, 10);
	}
	else
	{
		DrawDebugLine(GetWorld(), backTip, backEnd, FColor(255, 0, 0), false, -1, 0, 10);
	}
	if (leftHit)
	{
		DrawDebugLine(GetWorld(), leftTip, leftHR.ImpactPoint, FColor(0, 0, 255), false, -1, 0, 10);
	}
	else
	{
		DrawDebugLine(GetWorld(), leftTip, leftEnd, FColor(255, 0, 0), false, -1, 0, 10);
	}
	if (rightHit)
	{
		DrawDebugLine(GetWorld(), rightTip, rightHR.ImpactPoint, FColor(0, 0, 255), false, -1, 0, 10);
	}
	else
	{
		DrawDebugLine(GetWorld(), rightTip, rightEnd, FColor(255, 0, 0), false, -1, 0, 10);
	}*/


	/*Pitch Equation*/
	if (frontHit && backHit)
	{
		FVector tempVec2;
		float frontDist;
		float backDist;
		(frontHR.ImpactPoint - frontTip).ToDirectionAndLength(tempVec2, frontDist);
		(backHR.ImpactPoint - backTip).ToDirectionAndLength(tempVec2, backDist);
		float vertDiff = backDist - frontDist;
		float horiDiff = boundingBoxVector.X * 2;
		float tanDiff = vertDiff / horiDiff;
		float angle = FMath::Atan(tanDiff);
		angle = FMath::RadiansToDegrees(angle);
		angle = FMath::CeilToFloat(angle);
		supposedRotation.Pitch = angle;
	}
	else if (!frontHit && !backHit)
	{
		supposedRotation.Pitch -= 0.5f;
	}

	/*Roll Equation*/
	if (rightHit && leftHit)
	{
		FVector tempVec3;
		float rightDist;
		float leftDist;
		(rightHR.ImpactPoint - rightTip).ToDirectionAndLength(tempVec3, rightDist);
		(leftHR.ImpactPoint - leftTip).ToDirectionAndLength(tempVec3, leftDist);
		float vertDiff2 = rightDist - leftDist;
		float horiDiff2 = boundingBoxVector.Y * 2;
		float tanDiff2 = vertDiff2 / horiDiff2;
		float angle2 = FMath::Atan(tanDiff2);
		angle2 = FMath::RadiansToDegrees(angle2);
		angle2 = FMath::CeilToFloat(angle2);
		supposedRotation.Roll = angle2;
	}
}

void AVehicle_Char::DampenRotation(float DeltaSeconds)
{
	float dampYawRate = 90.f;
	float dampPitchRate = 270.f;
	float dampRollRate = 90.f;


	if (meshCurrentRotation.Yaw < supposedRotation.Yaw)
	{
		meshCurrentRotation.Yaw += dampYawRate*DeltaSeconds;
		if (meshCurrentRotation.Yaw > supposedRotation.Yaw)
		{
			meshCurrentRotation.Yaw = supposedRotation.Yaw;
		}
	}
	else if (meshCurrentRotation.Yaw > supposedRotation.Yaw)
	{
		meshCurrentRotation.Yaw -= dampYawRate*DeltaSeconds;
		if (meshCurrentRotation.Yaw < supposedRotation.Yaw)
		{
			meshCurrentRotation.Yaw = supposedRotation.Yaw;
		}
	}

	if (meshCurrentRotation.Pitch < supposedRotation.Pitch)
	{
		meshCurrentRotation.Pitch += dampPitchRate*DeltaSeconds;
		if (meshCurrentRotation.Pitch > supposedRotation.Pitch)
		{
			meshCurrentRotation.Pitch = supposedRotation.Pitch;
		}
	}
	else if (meshCurrentRotation.Pitch > supposedRotation.Pitch)
	{
		meshCurrentRotation.Pitch -= dampPitchRate*DeltaSeconds;
		if (meshCurrentRotation.Pitch < supposedRotation.Pitch)
		{
			meshCurrentRotation.Pitch = supposedRotation.Pitch;
		}
	}

	if (meshCurrentRotation.Roll < supposedRotation.Roll)
	{
		meshCurrentRotation.Roll += dampRollRate*DeltaSeconds;
		if (meshCurrentRotation.Roll > supposedRotation.Roll)
		{
			meshCurrentRotation.Roll = supposedRotation.Roll;
		}
	}
	else if (meshCurrentRotation.Roll > supposedRotation.Roll)
	{
		meshCurrentRotation.Roll -= dampRollRate*DeltaSeconds;
		if (meshCurrentRotation.Roll < supposedRotation.Roll)
		{
			meshCurrentRotation.Roll = supposedRotation.Roll;
		}
	}
}




void AVehicle_Char::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (GetActorLocation().Z >= warningHeightZ)
	{
		DisplayMessage("You Are Flying Too High.", DeltaSeconds, FColor::Yellow);
	}
	if (GetActorLocation().Z >= killHeightZ)
	{
		bDeath = true;
	}

	if (bStillInAir && GetVelocity().Z >= 0.f)
	{
		bStillInAir = false;
	}

	if (currentState == State_Transforming)
	{
		UpdateTransformationInput(DeltaSeconds);
	}
	else if (currentState == State_Car)
	{
		//if (!bStillInAir)
		//{
		UpdateCarInput(DeltaSeconds);
		//}
		if (currentSpeed > carMaxSpeed)
		{
			currentSpeed -= carAcceleration*DeltaSeconds;
			currentSpeed -= overSpeedDecay*DeltaSeconds;
		}
		if (bAutoAccelerate && !bIsFuelImmune)
		{
			currentFuel -= carFuelConsumption*DeltaSeconds;
		}
	}
	else if (currentState == State_Plane)
	{
		UpdatePlaneInput(DeltaSeconds);
		if (currentSpeed > planeMaxSpeed)
		{
			currentSpeed -= planeAcceleration*DeltaSeconds;
			currentSpeed -= overSpeedDecay*DeltaSeconds;
		}
		if (bAutoAccelerate && !bIsFuelImmune)
		{
			currentFuel -= planeFuelConsumption*DeltaSeconds;
		}
	}
	else if (currentState == State_Bike)
	{
		UpdateBikeInput(DeltaSeconds);
		if (currentSpeed > bikeMaxSpeed)
		{
			currentSpeed -= bikeAcceleration*DeltaSeconds;
			currentSpeed -= overSpeedDecay*DeltaSeconds;
		}
		if (bAutoAccelerate && !bIsFuelImmune)
		{
			currentFuel -= bikeFuelConsumption*DeltaSeconds;
		}
	}
	//DisplayMessage(FString::SanitizeFloat(currentFuel), DeltaSeconds, FColor::Blue);
	if (currentFuel <= 0.f)
	{
		currentFuel = 0.f;
		bDeath = true;
		bAutoAccelerate = false;
	}
	DampenRotation(DeltaSeconds);
	GetMesh()->SetRelativeRotation(meshCurrentRotation);
	/*Set Rotation and Add Movement*/
	SetActorRotation(currentRotation);
	float grossSpeed = currentSpeed;
	if (bIsBoosted)
	{
		grossSpeed += currentBoostedSpeed;
	}
	AddMovementInput(GetActorRotation().Vector(), grossSpeed, true);
	CharacterMovement->MaxWalkSpeed = ((grossSpeed * 1000000) / 3600) / 30;
	CharacterMovement->MaxFlySpeed = ((grossSpeed * 1000000) / 3600) / 30;

	/*Debugging Purposes
	FString status_Str;
	status_Str = "Current Score: ";
	status_Str.Append(FString::FromInt(currentScore));

	GEngine->AddOnScreenDebugMessage(-1, DeltaSeconds, FColor::Blue, status_Str);

	FString speed_Str;
	//speed_Str = "MaxWalkSpeed: ";
	speed_Str = "GrossSpeed: ";
	//speed_Str.Append(FString::SanitizeFloat(CharacterMovement->MaxWalkSpeed));
	speed_Str.Append(FString::SanitizeFloat(grossSpeed));

	GEngine->AddOnScreenDebugMessage(-1, DeltaSeconds, FColor::Red, speed_Str);

	FString pitch_Str;
	pitch_Str = "Current Speed: ";
	pitch_Str.Append(FString::SanitizeFloat(currentSpeed));

	GEngine->AddOnScreenDebugMessage(-1, DeltaSeconds, FColor::Green, pitch_Str);*/

	if (bIsCollidingWithWalls)
	{
		//DisplayMessage("Colliding with something", DeltaSeconds, FColor::Red);
		bIsCollidingWithWalls = false;
	}
}

void AVehicle_Char::BeginPlay()
{
	Super::BeginPlay();

	FVector tempOriginVec;
	GetActorBounds(true, tempOriginVec, boundingBoxVector);//Get Actor Bounds when Init
}

void AVehicle_Char::ReceiveHit(class UPrimitiveComponent* MyComp, AActor* Other, class UPrimitiveComponent* OtherComp,
	bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::ReceiveHit(MyComp, Other, OtherComp, bSelfMoved, HitLocation, HitNormal, NormalImpulse, Hit);
	//DisplayMessage("Receive Hit", 1.f, FColor::Red);
	if (Cast<AWorldInteraction_Char>(Other))
	{
		return;
	}
	bIsCollidingWithWalls = true;
	if (HitNormal.Z != 1.f)
	{
		FVector tempVec = HitLocation - GetActorLocation();
		FVector tempDir;
		float tempLength;
		tempVec.ToDirectionAndLength(tempDir, tempLength);
		FVector actualDir;
		actualDir.X = -tempDir.X;
		actualDir.Y = -tempDir.Y;
		actualDir.Z = 0.f;
		if (currentSpeed <= 50.f)
		{
			AddMovementInput(actualDir, 50.f, true);
		}
		else{
			AddMovementInput(actualDir, currentSpeed, true);
		}
	}
}

void AVehicle_Char::ReceiveActorBeginOverlap(AActor* OtherActor)
{
	Super::ReceiveActorBeginOverlap(OtherActor);
	//DisplayMessage("Begin Overlap", 1.f, FColor::Green);
}

void AVehicle_Char::ReceiveActorEndOverlap(AActor* OtherActor)
{
	Super::ReceiveActorEndOverlap(OtherActor);
	//DisplayMessage("End Overlap", 1.f, FColor::Blue);
}