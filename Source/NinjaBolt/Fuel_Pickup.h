// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Pickup_Char.h"
#include "Fuel_Pickup.generated.h"

/**
 * 
 */
UCLASS()
class NINJABOLT_API AFuel_Pickup : public APickup_Char
{
	GENERATED_UCLASS_BODY()
	

	/*Amount of Fuel the player receives upon getting this pickup*/
	UPROPERTY(EditAnywhere, category = "custom")
		int32 value;

	/*Collision Function*/
	virtual void PickupAction(APlayer_Char* hitPlayer) override;
};
