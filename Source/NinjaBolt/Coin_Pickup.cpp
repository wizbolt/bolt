// Fill out your copyright notice in the Description page of Project Settings.

#include "NinjaBolt.h"
#include "Coin_Pickup.h"


ACoin_Pickup::ACoin_Pickup(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	value = 1;
}

void ACoin_Pickup::PickupAction(APlayer_Char* hitPlayer)
{
	if (value < 1)
	{
		return;
	}
	hitPlayer->AddScore(value);
}
