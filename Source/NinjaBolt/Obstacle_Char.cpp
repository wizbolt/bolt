// Fill out your copyright notice in the Description page of Project Settings.

#include "NinjaBolt.h"
#include "Obstacle_Char.h"


AObstacle_Char::AObstacle_Char(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	bAlreadyActivated = false;
	speedDecreaseValue = 60.f;
}


void AObstacle_Char::HitPlayer(APlayer_Char* otherPlayer)
{
	CheckPlayerInvincibility(otherPlayer);
}

void AObstacle_Char::BeginOverlapPlayer(APlayer_Char* otherPlayer)
{
	CheckPlayerInvincibility(otherPlayer);
}

void AObstacle_Char::EndOverlapPlayer(APlayer_Char* otherPlayer)
{
	CheckPlayerInvincibility(otherPlayer);
}

void AObstacle_Char::BelowPlayer(APlayer_Char* otherPlayer)
{
	CheckPlayerInvincibility(otherPlayer);
}

void AObstacle_Char::CheckPlayerInvincibility(APlayer_Char* otherPlayer)
{
	if (otherPlayer->bIsInvincible)
	{
		HitInvinciblePlayer();
	}
	else
	{
		HitNormalPlayer(otherPlayer);
	}
}

void AObstacle_Char::HitInvinciblePlayer()
{
	DisplayMessage("Hit an invincible player; Destroying obstacle.", 1.f, FColor::Green);
	Destroy();
}

void AObstacle_Char::HitNormalPlayer(APlayer_Char* hitPlayer)
{
	hitPlayer->Destroy();
}