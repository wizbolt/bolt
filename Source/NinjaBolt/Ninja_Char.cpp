// Fill out your copyright notice in the Description page of Project Settings.

#include "NinjaBolt.h"
#include "Ninja_Char.h"


ANinja_Char::ANinja_Char(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	SetActorEnableCollision(true);
}


void ANinja_Char::DisplayMessage(FString inString, float duration, FColor colour)
{
	GEngine->AddOnScreenDebugMessage(-1, duration, colour, inString);
}

