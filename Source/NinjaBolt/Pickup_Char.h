// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "WorldInteraction_Char.h"

#include "Player_Char.h"

#include "Pickup_Char.generated.h"

/**
 * 
 */
UCLASS()
class NINJABOLT_API APickup_Char : public AWorldInteraction_Char
{
	GENERATED_UCLASS_BODY()

	/*Override BeginOverlapPlayer because the pickup will overlap and not hit the player*/
	virtual void BeginOverlapPlayer(APlayer_Char* otherPlayer) override;

	virtual void PickupAction(APlayer_Char* hitPlayer);
};
