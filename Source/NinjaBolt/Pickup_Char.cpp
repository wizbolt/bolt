// Fill out your copyright notice in the Description page of Project Settings.

#include "NinjaBolt.h"
#include "Pickup_Char.h"


APickup_Char::APickup_Char(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

void APickup_Char::BeginOverlapPlayer(APlayer_Char* otherPlayer)
{
	PickupAction(otherPlayer);
	Destroy();
}

void APickup_Char::PickupAction(APlayer_Char* hitPlayer)
{
	DisplayMessage("Pickup has no action.", 1.f, FColor::Red);
	return;
}