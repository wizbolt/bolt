// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Ninja_Char.h"

#include "WorldInteraction_Char.h"

#include "WorldObjectGenerator.generated.h"

USTRUCT()
struct FSpawnDetails
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Custom")
		UClass* spawnClass;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Custom")
		float floatChanceOfSpawn;

	FSpawnDetails()
	{
		spawnClass = NULL;
		floatChanceOfSpawn = 1.f;
	}
};

/**
 * 
 */
UCLASS()
class NINJABOLT_API AWorldObjectGenerator : public ANinja_Char
{
	GENERATED_UCLASS_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Custom")
		TArray<FSpawnDetails> spawnList;
		//FSpawnDetails spawnList;

	virtual void BeginPlay() override;
};
