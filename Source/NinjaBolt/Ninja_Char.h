// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "Ninja_Char.generated.h"

/**
 *
 */
UCLASS()
class NINJABOLT_API ANinja_Char : public ACharacter
{
	GENERATED_UCLASS_BODY()

	/*Blueprint Callable function for on screen debugging*/
	UFUNCTION(BlueprintCallable, Category = "custom")
		void DisplayMessage(FString inString, float duration, FColor colour);	
};
