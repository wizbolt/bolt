// Fill out your copyright notice in the Description page of Project Settings.

#include "NinjaBolt.h"
#include "Portal_Char.h"





APortal_Char::APortal_Char(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
}

void APortal_Char::BeginOverlapPlayer(APlayer_Char* otherPlayer)
{
	ForceActorTransform(otherPlayer);
}

void APortal_Char::ForceActorTransform(APlayer_Char* otherPlayer)
{
	AVehicle_Char* hitVehicle;
	hitVehicle = Cast<AVehicle_Char>(otherPlayer);
	if (!hitVehicle)
	{
		return;
	}
	hitVehicle->ForceRandomTransform();
}