// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Pickup_Char.h"
#include "Invincible_Pickup.generated.h"

/**
 * 
 */
UCLASS()
class NINJABOLT_API AInvincible_Pickup : public APickup_Char
{
	GENERATED_UCLASS_BODY()
	
	/*Duration of invincibility the player receives upon getting this pickup*/
	UPROPERTY(EditAnywhere, category = "custom")
		float duration;

	/*Collision Function*/
	virtual void PickupAction(APlayer_Char* hitPlayer) override;
};
