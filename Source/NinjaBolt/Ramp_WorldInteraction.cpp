// Fill out your copyright notice in the Description page of Project Settings.

#include "NinjaBolt.h"
#include "Ramp_WorldInteraction.h"




ARamp_WorldInteraction::ARamp_WorldInteraction(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	speedBoostAmount = 120.f;
}

void ARamp_WorldInteraction::BelowPlayer(APlayer_Char* otherPlayer)
{
	otherPlayer->AddBoostedSpeed(speedBoostAmount);
}