// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "WorldInteraction_Char.h"
#include "Ramp_WorldInteraction.generated.h"

/**
 * 
 */
UCLASS()
class NINJABOLT_API ARamp_WorldInteraction : public AWorldInteraction_Char
{
	GENERATED_UCLASS_BODY()


	/*Amount of boosted speed the Player will get upon interacting with this*/
	float speedBoostAmount;

	/*Override Functions*/
	virtual void BelowPlayer(APlayer_Char* otherPlayer) override;
};