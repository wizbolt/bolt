// Fill out your copyright notice in the Description page of Project Settings.

#include "NinjaBolt.h"
#include "WorldObjectGenerator.h"




AWorldObjectGenerator::AWorldObjectGenerator(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{

}

void AWorldObjectGenerator::BeginPlay()
{
	Super::BeginPlay();
	if (spawnList.Num() > 0)
	{
		
		for (int32 i = 0; i < spawnList.Num(); i++)
		{
			if (!spawnList[i].spawnClass)
			{
				DisplayMessage("NULL spawn class", 3.f, FColor::Red);
				continue;
			}
			float randFloat = FMath::FRand();
			if (randFloat <= spawnList[i].floatChanceOfSpawn)
			{
				//TODO:spawn actor here
				DisplayMessage("Spawning Something", 3.f, FColor::Blue);
				break;
			}
			DisplayMessage("Loop Ended", 3.f, FColor::Green);
		}
	}
}