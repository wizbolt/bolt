// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Ninja_Char.h"
#include "Player_Char.generated.h"

/**
 * 
 */
UCLASS()
class NINJABOLT_API APlayer_Char : public ANinja_Char
{
	GENERATED_UCLASS_BODY()

	UPROPERTY(BlueprintReadWrite, category = "custom")
	int32 currentScore;
	
	void AddScore(int32 inScore);
	void ReduceScore(int32 inScore);

	/*Uniform Speed across all modes*/
	UPROPERTY(BlueprintReadWrite, category = "Custom, Movement")
		float currentSpeed;
	UPROPERTY(BlueprintReadWrite, category = "Custom, Movement")
		float currentBoostedSpeed;
	float boostedSpeedDecay;
	UPROPERTY(BlueprintReadWrite, category = "Custom, Movement")
		bool bIsBoosted;

	void AddBoostedSpeed(float boostedSpeed);
	void ReduceSpeed(float reducedSpeed);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Custom, Fuel")
		float currentFuel;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Custom, Fuel")
		float maxFuel;

	void AddFuel(float inFuel);
	void ReduceFuel(float inFuel);

	bool bIsFuelImmune;

	void SetFuelImmunity(float duration);
	void StopFuelImmunity();

	bool bIsInvincible;

	void SetInvincibility(float duration);
	void StopInvincibility();

	virtual void Tick(float DeltaSeconds) override;
};
