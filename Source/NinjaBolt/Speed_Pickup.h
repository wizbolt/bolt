// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Pickup_Char.h"
#include "Speed_Pickup.generated.h"

/**
 * 
 */
UCLASS()
class NINJABOLT_API ASpeed_Pickup : public APickup_Char
{
	GENERATED_UCLASS_BODY()
	
	/*Values for pickup*/
	UPROPERTY(EditAnywhere, category = "custom")
		int32 speedIncreaseValue;
	
	virtual void PickupAction(APlayer_Char* hitPlayer) override;
};
