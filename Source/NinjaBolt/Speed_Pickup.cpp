// Fill out your copyright notice in the Description page of Project Settings.

#include "NinjaBolt.h"
#include "Speed_Pickup.h"


ASpeed_Pickup::ASpeed_Pickup(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	speedIncreaseValue = 120;
}

void ASpeed_Pickup::PickupAction(APlayer_Char* hitPlayer)
{
	hitPlayer->AddBoostedSpeed(speedIncreaseValue);
}


